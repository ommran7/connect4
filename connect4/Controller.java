package connect4;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

//import javafx.util.Pair;


public class Controller {

    private Connect4Game board;
    private Connect4Game connect4Game;

    public int profitCondition;
    public void condititon()

    {

        Scanner s = new Scanner(System.in);
        System.out.print("Enter the profit condition: ");
        profitCondition = s.nextInt();

    }


    char computer = 'o';
    char human = 'x';

    public Controller(int profitCondition) {
        this.profitCondition = profitCondition;
        computer = 'o';
        human = 'x';
        board = new Connect4Game(6, 6, profitCondition);
    }

    public void play() {
        System.out.println(board);
        while (true) {
            humanPlay();
            System.out.println(board);

            if (board.isWin(human)) {
                System.out.println("Human wins");
                break;
            }
            if (board.isWithdraw()) {
                System.out.println("Draw");
                break;
            }
            computerPlay();
            System.out.println("_____Computer Turn______");
            System.out.println(board);
            if (board.isWin(computer)) {
                System.out.println("Computer wins!");
                break;
            }
            if (board.isWithdraw()) {
                System.out.println("Draw");
                break;
            }
        }

    }

    //         ************** YOUR CODE HERE ************            \\
    private void computerPlay() {
        List<Object> result = maxMove(board, Integer.MIN_VALUE, Integer.MAX_VALUE, 10); // Increase the depth as needed

        if (result.get(1) instanceof Connect4Game) {
            board = (Connect4Game) result.get(1);
        }
    }


    /**
     * Human plays
     *
     * @return the column the human played in
     */
    private void humanPlay() {
        Scanner s = new Scanner(System.in);
        int col;

        while (true) {
            System.out.print("Enter column (1-" + board.getWidth() + ") or 'w' to withdraw: ");
            String input = s.next();
            System.out.println();

            if (input.equalsIgnoreCase("w")) {
                // Withdraw a piece
                System.out.print("Enter column to withdraw from (1-" + board.getWidth() + "): ");
                int withdrawColumn = s.nextInt();
                if (withdrawColumn >= 1 && withdrawColumn <= board.getWidth()) {
                    // Pull the piece from the top of the column
                    board.pull(human, withdrawColumn - 1);
                    return;
                } else {
                    System.out.println("Invalid Column: out of range " + board.getWidth() + ", try again");
                }
            } else {
                // Enter a specific column
                // Inside the humanPlay method
                col = Integer.parseInt(input);
                if ((col >= 1) && (col <= board.getWidth())) {
                    System.out.print("Do you want to (1) play or (2) swap? Enter 1 or 2: ");
                    int choice = s.nextInt();

                    if (choice == 1) {
                        // Make a regular move
                        if (board.play(human, col - 1)) {
                            return;
                        } else {
                            System.out.println("Invalid Column: Column " + col + " is full!, try again");
                        }
                    } else if (choice == 2) {
                        // Initiate a swap
                        checkAndPerformSwap(col, choice);
                        return;
                    } else {
                        System.out.println("Invalid choice. Enter 1 to play or 2 to swap.");
                    }
                } else {
                    System.out.println("Invalid Column: out of range " + board.getWidth() + ", try again");
                }
            }
        }
    }


                private boolean isValidColumn(int col) {
        return col >= 1 && col <= board.getWidth();
    }


    private void checkAndPerformSwap(int col, int choice) {
        if (choice == 1) {
            // Make a regular move
            if (board.play(human, col - 1)) {
                return;
            } else {
                System.out.println("Invalid Column: Column " + col + " is full!, try again");
                return;  // Added to exit the method when the column is full
            }
        } else if (choice == 2) {
            // Initiate a swap
            Scanner s = new Scanner(System.in);
            System.out.print("Enter the column to swap with: ");
            int swapColumn = s.nextInt();
            if (isValidColumn(swapColumn) && swapColumn != col) {
                System.out.print("Enter the level to swap (1-" + Math.min(board.topPieceIndex[col - 1], board.topPieceIndex[swapColumn - 1]) + "): ");
                int swapLevel = s.nextInt();
                if (swapLevel >= 1 && swapLevel <= Math.min(board.topPieceIndex[col - 1], board.topPieceIndex[swapColumn - 1])) {
                    board.swapPieces(col - 1, swapColumn - 1);
                    return;
                } else {
                    System.out.println("Invalid level: out of range, try again");
                }
            } else {
                System.out.println("Invalid Column: out of range or same as the original column, try again");
            }
        } else {
            System.out.println("Invalid choice. Enter 1 to play or 2 to swap.");
        }
    }






    private List<Object> maxMove(Connect4Game b, int alpha, int beta, int depth) {
        // the fuction returns list of object the first object is the evaluation (type Integer), the second is the state with the max evaluation
//         ************** YOUR CODE HERE ************            \\




        if (depth == 0 || b.isFinished()) {
            return new ArrayList<>(Arrays.asList(b.evaluate(computer), b));
        }

        int bestValue = Integer.MIN_VALUE;
        Connect4Game bestMoveState = null;

        for (Connect4Game nextState : b.allNextMoves(computer)) {
            int value = (int) minMove(nextState, alpha, beta, depth - 1).get(0);

            if (value > bestValue) {
                bestValue = value;
                bestMoveState = nextState;
            }

            alpha = Math.max(alpha, bestValue);
            if (alpha >= beta) {
                break; // Beta cut-off
            }
        }

        List<Object> result = new ArrayList<>();
        result.add(bestValue);
        result.add(bestMoveState);
        return result;
    }



    private List<Object> minMove(Connect4Game b, int alpha, int beta, int depth) {
        // the fuction returns list of object the first object is the evaluation (type Integer), the second is the state with the min evaluation
//         ************** YOUR CODE HERE ************            \\


            // Check if depth limit is reached or game is finished
        if (depth == 0 || b.isFinished()) {
            return new ArrayList<>(Arrays.asList(b.evaluate(human), b));
        }

        int bestValue = Integer.MAX_VALUE;
        Connect4Game bestMoveState = null;

        for (Connect4Game nextState : b.allNextMoves(human)) {
            int value = (int) maxMove(nextState, alpha, beta, depth - 1).get(0);

            if (value < bestValue) {
                bestValue = value;
                bestMoveState = nextState;
            }

            beta = Math.min(beta, bestValue);
            if (alpha >= beta) {
                break; // Alpha cut-off
            }
        }

        List<Object> result = new ArrayList<>();
        result.add(bestValue);
        result.add(bestMoveState);
        return result;
    }



        public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        System.out.print("Enter the profit condition: ");
        int profitCondition = s.nextInt();

        Controller g = new Controller(profitCondition);

        g.play();
    }
}
